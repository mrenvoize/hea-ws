package Hea::Data;

use Modern::Perl;
use utf8;

my $country_map = {
    'Brasil'                  => 'Brazil',
    'Cabo Verde'              => 'Cape Verde',
    'Česká republika'         => 'Czech Republic',
    'country'                 => '',
    'España'                  => 'Spain',
    'Espanya'                 => 'Spain',
    'Japon'                   => 'Japan',
    'KINGDOM OF SAUDI ARABIA' => 'Saudi Arabia',
    'Kobe, Japan'             => 'Japan',
    'Lao PDR'                 => 'Laos',
    'Macau SAR'               => 'Macau',
    'MX'                      => 'Mexico',
    'Nederland'               => 'Netherlands',
    'Österreich'              => 'Austria',
    'Portugual'               => 'Portugal',
    'PT'                      => 'Portugal',
    'República Dominicana'    => 'Dominican Rep.',
    'República Dominicana.'   => 'Dominican Rep.',
    'Russian Fed.'            => 'Russia',
    'Rwanda.'                 => 'Rwanda',
    'Slovensko'               => 'Slovakia',
    'spain'                   => 'Spain',
    'Srilanka'                => 'Sri Lanka',
    'Suisse'                  => 'Switzerland',
    'Trinidad & Tob.'         => 'Trinidad and Tobago',
    'Tunisie'                 => 'Tunisia',
    'Türkiye'                 => 'Turkey',
    'UK'                      => 'United Kingdom',
    'US'                      => 'USA',
    'United States'           => 'USA',
    'Vila de São Sebastião'   => 'Portugal',
    'ΕΛΛΑΔΑ'                  => 'Greece',
    'Россия'                  => 'Russia',
    'Україна'                 => 'Ukraine',
    'ՀՀ'                      => 'Armenia',
};

sub normalize_country {
    my ( $country ) = @_;

    $country =~ s/^\s*|\s*$//g; # trim
    $country =~ s/^(.)/\u$1/; # Uppercase first letter

    return $country_map->{$country}
        if exists $country_map->{$country};

    return $country;
}

1;
